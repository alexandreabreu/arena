class LutadoresController < ApplicationController
  # GET /lutadores
  # GET /lutadores.json
  def index
    @lutadores = (Lutador.count > 24) ? Lutador.all(limit: 24, offset: rand(Lutador.count - 24)) : Lutador.all
    @lutadores.reverse!
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lutadores }
    end
  end

  # GET /lutadores/1
  # GET /lutadores/1.json
  def show
    @lutador = Lutador.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lutador }
    end
  end

  # GET /lutadores/new
  # GET /lutadores/new.json
  def new
    @lutador = Lutador.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lutador }
    end
  end

  # GET /lutadores/1/edit
  def edit
    @lutador = Lutador.find(params[:id])
  end

  # POST /lutadores
  # POST /lutadores.json
  def create
    @lutador = Lutador.new(params[:lutador])

    respond_to do |format|
      if @lutador.save
        format.html { redirect_to @lutador, notice: 'Lutador was successfully created.' }
        format.json { render json: @lutador, status: :created, location: @lutador }
      else
        format.html { render action: "new" }
        format.json { render json: @lutador.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lutadores/1
  # PUT /lutadores/1.json
  def update
    @lutador = Lutador.find(params[:id])

    respond_to do |format|
      if @lutador.update_attributes(params[:lutador])
        format.html { redirect_to @lutador, notice: 'Lutador was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lutador.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lutadores/1
  # DELETE /lutadores/1.json
  def destroy
    @lutador = Lutador.find(params[:id])
    @lutador.destroy

    respond_to do |format|
      format.html { redirect_to lutadores_url }
      format.json { head :no_content }
    end
  end
  
  def gen
    Lutador.novo_aleatorio.save
    redirect_to lutadores_path
  end
  
  
end
