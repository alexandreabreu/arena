class Lutador < ActiveRecord::Base
  attr_accessible :ataque, :classe, :defesa, :idade, :imagem, :nome, :prototipo_id, :taxa_ataque, :taxa_defesa, :taxa_tecnica, :tecnica, :apelido, :prototipo
  belongs_to :prototipo
  
  def total
    ataque + defesa + tecnica
  end
  
  def self.novo_aleatorio(prototipo = Prototipo.where(classe: Prototipo::SERVANO).first , sexo = "r")
    sexo = ((rand(2) == 0) ? "f" : "m") if (sexo == "r")
    Lutador.new do |l|
      l.prototipo = prototipo
      l.classe = prototipo.classe
      l.nome, l.apelido = Nome.randomico prototipo, sexo
      l.imagem = Retrato.randomico prototipo, sexo
      l.idade = prototipo.idade_minima + rand(5)
      l.ataque = prototipo.ataque + rand(10)
      l.defesa = prototipo.defesa + rand(10)
      l.tecnica = prototipo.tecnica + rand(10)
      l.taxa_ataque = prototipo.taxa_ataque + rand(3)
      l.taxa_defesa = prototipo.taxa_defesa + rand(3)
      l.taxa_tecnica = prototipo.taxa_tecnica + rand(3)
    end
  end  
  
end
