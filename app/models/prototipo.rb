#!/bin/env ruby
# encoding: utf-8

class Prototipo < ActiveRecord::Base
  attr_accessible :classe, :ataque, :defesa, :idade_minima, :longevidade, :imagem, :taxa_ataque, :taxa_defesa, :taxa_tecnica, :tecnica
  has_many :lutadores
  has_many :nomes
  has_many :retratos
  
  SERVANO, ELFO, NEFANO, ORC, NIHONANO, BRUTO, BARBARO, ARCANO, DEMONIO, SELVAGEM, FOTONOIDE, DEMONOIDE, ESPARTANO, SINOANO, FELINO = 
  ["Servano", "Elfo", "Nefano", "Orc", "Nihonano", "Bruto", "Bárbaro", "Arcano", "Demônio", "Selvagem", "Fotonóide", "Demonóide", "Espartano", "Sinoano", "Felino"] 
  
  CLASSES = [SERVANO, ELFO, NEFANO, ORC, NIHONANO, BRUTO, BARBARO, ARCANO, DEMONIO, SELVAGEM, FOTONOIDE, DEMONOIDE, ESPARTANO, SINOANO, FELINO]
  
end
