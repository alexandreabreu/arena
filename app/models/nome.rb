class Nome < ActiveRecord::Base
  attr_accessible :palavra, :prototipo_id, :tipo, :prototipo, :genero
  belongs_to :prototipo
  
  validates :palavra, :tipo, presence: true
  
  MASCULINO = "m"
  FEMININO = "f"
  SOBRENOME = "sobrenome"
  APELIDO_MASCULINO = "apelido_masculino"
  APELIDO_FEMININO = "apelido_feminino"
  ADJETIVO = "adjetivo"
  GENERO_M = "m"
  GENERO_F = "f"
  GENERO_U = "u"
  
  TIPOS = [MASCULINO, FEMININO, SOBRENOME, APELIDO_MASCULINO, APELIDO_FEMININO, ADJETIVO]
  GENEROS = [GENERO_M, GENERO_F, GENERO_U]
  
  
  
  def self.randomico(prototipo = Prototipo.where(classe: Prototipo::SERVANO).first, sexo = self::GENERO_M)
    conjunto = Nome.where(prototipo_id: prototipo)
    if (sexo == Nome::MASCULINO) 
      primeiros = conjunto.where(tipo: Nome::MASCULINO)
      apelidos =  conjunto.where(tipo: Nome::APELIDO_MASCULINO)
    else
      primeiros = conjunto.where(tipo: Nome::FEMININO)
      apelidos =  conjunto.where(tipo: Nome::APELIDO_FEMININO)
    end 
    sobrenomes = conjunto.where(tipo: Nome::SOBRENOME)
    sobrenome = sobrenomes.first(offset: rand(sobrenomes.count)).palavra
    primeiro = primeiros.first(offset: rand(primeiros.count)).palavra
    apelido = apelidos.first(offset: rand(apelidos.count))
    if (apelido.genero.present? and !(rand(8) == 0))
      adjetivos = conjunto.where(tipo: Nome::ADJETIVO, genero: ["u", apelido.genero])
      adjetivo = adjetivos.first(offset: rand(adjetivos.count))
      apelido = "#{apelido.palavra} #{adjetivo.palavra}"
    else
      apelido = apelido.palavra  
    end
    ["#{primeiro} #{sobrenome}" , apelido]
  end
  
  
  
end
