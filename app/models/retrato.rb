class Retrato < ActiveRecord::Base
  attr_accessible :imagem, :prototipo_id, :sexo, :prototipo
  belongs_to :prototipo
  
  #TODO: corrigir sexos para m,f
  MASCULINO = "m"
  FEMININO = "f"
  
  def self.randomico(prototipo = Prototipo.where(classe: Prototipo::SERVANO).first, sexo = MASCULINO)
      retratos = where sexo: sexo, prototipo_id: prototipo
      retratos.first(offset: rand(retratos.count)).imagem
  end
  
  

end
