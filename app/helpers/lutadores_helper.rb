#!/bin/env ruby
# encoding: utf-8

module LutadoresHelper


def label_taxa(taxa)
  if (taxa % 2) == 1
    t = taxa + 1 
  else
    t = taxa
  end
  content_tag(:span, "+ #{taxa}", class: "label label#{t}")
end

def atributos(lutador)
  idade = content_tag(:td, "Idade") + content_tag(:td, lutador.idade) + content_tag(:td, link_to("Remover", lutador, method: :delete, class: "btn btn-danger btn-mini"))
  ataque = content_tag(:td, "Ataque") + content_tag(:td, lutador.ataque) + content_tag(:td, label_taxa(lutador.taxa_ataque))
  defesa = content_tag(:td, "Defesa") + content_tag(:td, lutador.defesa) + content_tag(:td, label_taxa(lutador.taxa_defesa))
  tecnica = content_tag(:td, "Técnica") + content_tag(:td, lutador.tecnica) + content_tag(:td, label_taxa(lutador.taxa_tecnica))
  tbody = content_tag(:tbody, content_tag(:tr, idade) + content_tag(:tr, ataque) + content_tag(:tr, defesa) + content_tag(:tr, tecnica))
  content_tag :table, tbody, class: "table table-condensed table-striped"
end



end
