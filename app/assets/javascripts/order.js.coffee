$.fn.order = (options) ->
    defaults = 
        maxSpan: 12
        rowClass: "row"
        containerClass: "container"
    options = $.extend(defaults, options)
    options["rowClass"] = "." + options["rowClass"]
    $.fn.order.insertRow = ->
        $.fn.order.currentRow = $("</div>").addClass(options["rowClass"])
        $("."+options["containerClass"]).append $.fn.order.currentRow
        $.fn.order.currentRow

    $.fn.order.rowTotal = (row)->
    	total = 0
    	$('[class*=span]', row).each ->
    		total += parseInt this.className.match(/span(\d+)/i)[1]
    	total
    $.fn.order.currentRow = if ($(options["rowClass"]).length == 0) then $.fn.order.insertRow() else $(options["rowClass"] + ":first")

    @each ->
        size =  parseInt this.className.match(/span(\d+)/i)[1]
        loop
        	break if (options["maxSpan"] - ($.fn.order.rowTotal($.fn.order.currentRow) + size)) >= 0
        	if $.fn.order.currentRow.next(options["rowClass"]).length == 0
        		$.fn.order.insertRow()
        	else
        		$.fn.order.currentRow = $.fn.order.currentRow.next(options["rowClass"])
        $.fn.order.currentRow.append(this)
