require 'test_helper'

class LutadoresControllerTest < ActionController::TestCase
  setup do
    @lutador = lutadores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lutadores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lutador" do
    assert_difference('Lutador.count') do
      post :create, lutador: { ataque: @lutador.ataque, classe: @lutador.classe, defesa: @lutador.defesa, idade: @lutador.idade, imagem: @lutador.imagem, nome: @lutador.nome, prototipo_id: @lutador.prototipo_id, taxa_ataque: @lutador.taxa_ataque, taxa_defesa: @lutador.taxa_defesa, taxa_tecnica: @lutador.taxa_tecnica, tecnica: @lutador.tecnica, titulo: @lutador.titulo }
    end

    assert_redirected_to lutador_path(assigns(:lutador))
  end

  test "should show lutador" do
    get :show, id: @lutador
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lutador
    assert_response :success
  end

  test "should update lutador" do
    put :update, id: @lutador, lutador: { ataque: @lutador.ataque, classe: @lutador.classe, defesa: @lutador.defesa, idade: @lutador.idade, imagem: @lutador.imagem, nome: @lutador.nome, prototipo_id: @lutador.prototipo_id, taxa_ataque: @lutador.taxa_ataque, taxa_defesa: @lutador.taxa_defesa, taxa_tecnica: @lutador.taxa_tecnica, tecnica: @lutador.tecnica, titulo: @lutador.titulo }
    assert_redirected_to lutador_path(assigns(:lutador))
  end

  test "should destroy lutador" do
    assert_difference('Lutador.count', -1) do
      delete :destroy, id: @lutador
    end

    assert_redirected_to lutadores_path
  end
end
