class CreateNomes < ActiveRecord::Migration
  def change
    create_table :nomes do |t|
      t.string :tipo
      t.string :palavra
      t.integer :prototipo_id

      t.timestamps
    end
  end
end
