class CreateLutadores < ActiveRecord::Migration
  def change
    create_table :lutadores do |t|
      t.integer :prototipo_id
      t.string :imagem
      t.string :nome
      t.string :apelido
      t.integer :classe
      t.integer :idade
      t.integer :ataque
      t.integer :defesa
      t.integer :tecnica
      t.integer :taxa_ataque
      t.integer :taxa_defesa
      t.integer :taxa_tecnica

      t.timestamps
    end
  end
end
