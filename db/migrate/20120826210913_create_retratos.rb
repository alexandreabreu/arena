class CreateRetratos < ActiveRecord::Migration
  def change
    create_table :retratos do |t|
      t.string :imagem
      t.string :sexo
      t.integer :prototipo_id

      t.timestamps
    end
  end
end
