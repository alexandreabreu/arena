class ChangeLutadorClasseFromIntegerToString < ActiveRecord::Migration
  def up
    remove_column :lutadores, :classe
    add_column :lutadores, :classe, :string
  end

  def down
    remove_column :lutadores, :classe
    add_column :lutadores, :classe, :integer
  end
end
