class CreatePrototipos < ActiveRecord::Migration
  def change
    create_table :prototipos do |t|
      t.string :classe
      t.integer :ataque
      t.integer :defesa
      t.integer :tecnica
      t.integer :taxa_ataque
      t.integer :taxa_defesa
      t.integer :taxa_tecnica
      t.integer :idade_minima
      t.integer :longevidade

      t.timestamps
    end
  end
end
