#!/bin/env ruby
# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



Prototipo.delete_all
Retrato.delete_all
Nome.delete_all
Lutador.delete_all

#---equilibrado
Prototipo.create classe: Prototipo::SERVANO,       ataque: 50, defesa: 50, tecnica: 50, idade_minima: 17, longevidade: 23, taxa_ataque: 4, taxa_defesa: 3, taxa_tecnica: 3

#---levemente desequilibrado
Prototipo.create classe: Prototipo::ELFO,          ataque: 50, defesa: 40, tecnica: 60, idade_minima: 17, longevidade: 28, taxa_ataque: 3, taxa_defesa: 3, taxa_tecnica: 4
Prototipo.create classe: Prototipo::NEFANO,        ataque: 40, defesa: 60, tecnica: 50, idade_minima: 20, longevidade: 22, taxa_ataque: 3, taxa_defesa: 4, taxa_tecnica: 3
Prototipo.create classe: Prototipo::ORC,           ataque: 60, defesa: 50, tecnica: 40, idade_minima: 20, longevidade: 25, taxa_ataque: 4, taxa_defesa: 3, taxa_tecnica: 3

Prototipo.create classe: Prototipo::NIHONANO,      ataque: 45, defesa: 45, tecnica: 60, idade_minima: 19, longevidade: 23, taxa_ataque: 3, taxa_defesa: 3, taxa_tecnica: 4
Prototipo.create classe: Prototipo::BRUTO,         ataque: 45, defesa: 60, tecnica: 45, idade_minima: 19, longevidade: 21, taxa_ataque: 3, taxa_defesa: 4, taxa_tecnica: 3
Prototipo.create classe: Prototipo::BARBARO,       ataque: 60, defesa: 45, tecnica: 45, idade_minima: 18, longevidade: 25, taxa_ataque: 4, taxa_defesa: 3, taxa_tecnica: 3

#---desequilibrio moderado
Prototipo.create classe: Prototipo::ARCANO,        ataque: 40, defesa: 40, tecnica: 70, idade_minima: 19, longevidade: 22, taxa_ataque: 2, taxa_defesa: 3, taxa_tecnica: 5
Prototipo.create classe: Prototipo::DEMONIO,       ataque: 40, defesa: 70, tecnica: 40, idade_minima: 26, longevidade: 24, taxa_ataque: 3, taxa_defesa: 5, taxa_tecnica: 2
Prototipo.create classe: Prototipo::SELVAGEM,      ataque: 70, defesa: 40, tecnica: 40, idade_minima: 18, longevidade: 22, taxa_ataque: 5, taxa_defesa: 2, taxa_tecnica: 3


#---desequilibrio forte
Prototipo.create classe: Prototipo::FOTONOIDE,     ataque: 35, defesa: 35, tecnica: 80, idade_minima: 20, longevidade: 22, taxa_ataque: 2, taxa_defesa: 2, taxa_tecnica: 6
Prototipo.create classe: Prototipo::DEMONOIDE,     ataque: 35, defesa: 80, tecnica: 35, idade_minima: 23, longevidade: 22, taxa_ataque: 2, taxa_defesa: 6, taxa_tecnica: 2
Prototipo.create classe: Prototipo::ESPARTANO,     ataque: 80, defesa: 35, tecnica: 35, idade_minima: 16, longevidade: 22, taxa_ataque: 6, taxa_defesa: 2, taxa_tecnica: 2

Prototipo.create classe: Prototipo::SINOANO,       ataque: 40, defesa: 30, tecnica: 80, idade_minima: 19, longevidade: 25, taxa_ataque: 4, taxa_defesa: 1, taxa_tecnica: 5
Prototipo.create classe: Prototipo::FELINO,        ataque: 80, defesa: 30, tecnica: 40, idade_minima: 17, longevidade: 25, taxa_ataque: 5, taxa_defesa: 1, taxa_tecnica: 4


def carregar_retratos(path)
  puts "======================================>  Carregando Retratos..."
  Prototipo.all.each do |proto|
    Dir.foreach("#{Rails.root}#{path}/faces/#{proto.classe.parameterize}/m")  do |m|
      r = Retrato.create(prototipo: proto, imagem: "faces/#{proto.classe.parameterize}/m/#{m}", sexo: Retrato::MASCULINO) unless m.scan(/\w+/).empty?
      puts "#{proto.classe}[#{r.sexo}]: #{r.imagem}" if r
    end
    Dir.foreach("#{Rails.root}#{path}/faces/#{proto.classe.parameterize}/f")  do |f|
      r = Retrato.create(prototipo: proto, imagem: "faces/#{proto.classe.parameterize}/f/#{f}", sexo: Retrato::FEMININO) unless f.scan(/\w+/).empty?
      puts "#{proto.classe}[#{r.sexo}]: #{r.imagem}" if r
    end
  end
end

def salvar_nomes(hsh)
  puts "======================================>  Salvando Nomes..."
  hsh.each do |classe, hash_com_tipos|
    hash_com_tipos.each do |tipo, ary|
      ary.each do |nome|
        if nome.class == String
          n = Nome.create tipo: tipo, prototipo: Prototipo.where(classe: classe).first, palavra: nome
          puts "#{n.prototipo.classe}[#{n.tipo}]: #{n.palavra}" 
        else
          n = Nome.create tipo: tipo, prototipo: Prototipo.where(classe: classe).first, palavra: nome[:palavra], genero: nome[:genero]
          puts "#{n.prototipo.classe}[#{n.tipo}]: #{n.palavra}"
        end
      end
    end
  end
end

nomes = Hash.new { |hash, key| hash[key] = {Nome::MASCULINO => [], Nome::FEMININO => [], Nome::SOBRENOME => [], Nome::APELIDO_MASCULINO => [], Nome::APELIDO_FEMININO => []} }

nomes[Prototipo::SERVANO][Nome::MASCULINO] = [ "Armando", "Amaril", "Amkar", "Anon", "Alex", "Alexis", "Azim", "Aaron", "Amir", "Ader", "Altair", "André", "Andrei",
"Aramis", "Andreas", "Alexandre", "Abraão", "Alberto", "Alessando", "Alexandro", "Amon", "Agenor", "Arthur", "Alan", "Alandro", "Alejandro", "Anderson", "Armed",
"Benício", "Berilo", "Banjo", "Brício", "Brígido", "Bino", "Bogar", "Breno", "Benjamim", "Bento", "Benito", "Baldo", "Baco", "Bartolomeu", "Balbo", "Baltazar",
"Bruno", "Brunei", "Bernardo", "Bóris", "Bodram", "Bakar", "Bertram", "Carlos", "Carles", "Cogar", "Cláudio", "Clodoaldo", "Crismon", "Cristiano", "Covano", "Ciro",
"Craven", "Carlic", "Cedro", "Cedir", "Cassandro", "Cervarius", "Carmeno", "Crivo", "Carlexandre", "Centavius", "Crato", "Crasso", "Ceto", "Carmelo", "Chivu", "Colário",
"Clóvis", "Cleomir", "Célio", "Cássio", "Cravo", "Cedrando", "Cális", "Cerento", "Caldrei", "Dráuzio", "Dário", "Dormendes", "Dodoria", "Drapião", "Dineo", "Dino",
"Elder", "Eul", "Eric", "Évon", "Élber", "Esdravos", "Ed", "Eduardo", "Etrel", "Fernando", "Fábio", "Fernão", "Fabrício", "Fabro", "Flávio", "Fandro", "Fratus", "Frank", 
"Forrest", "Fordax", "Flaviano", "Felipe", "Fauno", "Francis", "Gabriel", "Gustavo", "Gander", "Gilberto", "Gil", "Galendro", "Gálvio", "Geremário", "Gesmar", "Glauber", "Gae",
"Hélio", "Heliassus", "Heleno", "Hermes", "Hex", "Higor", "Hendro", "Ives", "Iuri", "Julio", "Julião", "Jorge", "Jorjano", "Jano", "Jáder", "Jair", "Jádson", "Jeremias",
"João", "Jekel", "Juliano", "Jovem", "Jarbas", "Josiel", "Jael", "Jordão", "Kléber", "Kaol", "Kelvin", "Kim", "Klaus", "Kongo", "Kel", "Lauro", "Líber", "Lidsen", "Leomir",
"Leonardo", "Ludovic", "Luiz", "Leandro", "Larri", "Lionel", "Lindomar", "Lizandro", "Mário", "Mauro", "Melquíades", "Modra", "Merlin", "Marcos", "Malvino", "Muandre", "Marcel", 
"Mariano", "Milton", "Nelson", "Nizam", "Norman", "Nilton", "Newton", "Nálber", "Nevandro", "Nortrom", "Nilvam", "Otto", "Oziel", "Opel", "Pedro", "Paulo", "Patrício",
"Pietro", "Pierre", "Pasman", "Postiga", "Prúcio", "Qumar", "Raul", "Rolando", "Rui", "Rony", "Ricardo", "Rodrigo", "Ramel", "Radamés", "Roderigo", "Ramiro", "Rômulo",
"Renan", "Rafael", "Ramon", "Reinaldo", "Reginaldo", "Roger", "Raimundo", "Renato", "Róbson", "Rogério", "Raed", "Rodusino", "Rogato", "Romeu", "Romero", "Romildo",
"Remo", "Rayan", "Rial", "Sérgio", "Serguei", "Saul", "Sávio", "Sandro", "Sandor", "Shiver", "Sorlei", "Silvio", "Salomão", "Sidnei", "Samuel", "Sebastião", "Saulo",
"Samir", "Salatiel", "Simão", "Salvador", "Silas", "Sansão", "Sean", "Serafino", "Segismundo", "Sérvulo", "Severo", "Sezefredo", "Severino", "Silvino", "Sílvaco",
"Túlio", "Tony", "Tandre", "Tanos", "Tião", "Tertuliano", "Tavion", "Tessel", "Tarpan", "William", "Zei", "Zael"]
nomes[Prototipo::SERVANO][Nome::FEMININO] = ["Alina", "Aline", "Alana", "Alandra", "Ana", "Andrea", "Avana", "Avelana", "Ariel", "Anamaria", "Aturja", "Aspásia",
"Abigail", "Ângela", "Angélica", "Ágata", "Alícia", "Alderita", "Adriana", "Amanda", "Ainá", "Abella", "Acácia", "Alena", "Alekena", "Alejandra", "Amara", "Amélia",
"Amy", "Anabel", "Alzira", "Alice", "Andressa", "Andrezza", "Beatriz", "Bruna", "Belatrix", "Bianca", "Briana", "Brama", "Balena", "Bia", "Bernadete", "Blanca", "Birpina",
"Cristina", "Cibele", "Clarissa", "Carola", "Cíntia", "Clara", "Cloé", "Cleonice", "Cleide", "Clorina", "Cécile", "Ceci", "Catrina", "Célica", "Celimena", "Cesarina",
"Corina", "Cassandra", "Camila", "Carolina", "Carla", "Carmen", "Carma", "Cleo", "Cirlina", "Cláudia", "Cora", "Dina", "Diana", "Daisy", "Dorothy", "Ellen", "Elvina", "Erina", 
"Elza", "Eliane", "Fernanda", "Flávia", "Flora", "Fiona", "Ferane", "Felícia", "Flamínea", "Galandra", "Gaia", "Gina", "Gall", "Helena", "Hustana", "Helga", "Hilda",
"Hévora", "Iliana", "Irma", "Igraine", "Jaina", "Joana", "Janaína", "Jordana", "Jamile", "Juliana", "Josi", "Jade", "Kezia", "Keyla", "Kenya", "Kali", "Kátia",
"Laura", "Lena", "Luiza", "Luma", "Liana", "Lorette", "Luana", "Luara", "Lóris", "Monique", "Mônica", "Morena", "Melina", "Melissa", "Marina", "Maria", "Mariana", "Malvina",
"Maura", "Norma", "Neusa", "Nadir", "Nuama", "Nilva", "Narvina", "Nam", "Opereta", "Olga", "Oníma", "Orana", "Paula", "Paloma", "Patrícia", "Pan", "Pamela", "Perrona",
"Priscila", "Pietra", "Paola", "Poliana", "Pollyane", "Paulete", "Pauline", "Palmira", "Pepita", "Penélope", "Pérola", "Paki", "Petrônia", "Policasta", "Potira",
"Raíssa", "Rayane", "Roberta", "Riana", "Renata", "Ruama", "Sabrina", "Soraia", "Sabatela", "Shirlei", "Shandra", "Silvia", "Silvana", "Samira", "Susan", "Tatiane",
"Thaís", "Tamires", "Tainá", "Telandra", "Tessália", "Yasmin", "Yndra", "Yvana", "Zeni", "Zélia", "Zoé"]
nomes[Prototipo::SERVANO][Nome::SOBRENOME] = ["Abreu", "Aldrin", "Avante", "Alzorano", "Avelaneira", "Áven", "Astradilho", "Abacur", "Áreas", "Armena", "Aggra",
"Balrona", "Bamer", "Bodriak", "Barbossa", "Belenense", "Barão", "Barra", "Caramel", "Canvan", "Catan", "Cooper", "Coilaur", "Chaves", "Cammo", "Castelo", "Cerp",
"Cevina", "Ceviche", "Campanha", "Cruze", "Carpet", "Cargono", "Cabral", "Cabrólios", "Craven", "Cinza", "Cera", "Dudinka", "Dramer", "Dalkog", "Drem", "Davio",
"Disha", "Drep", "Dorvam", "Drelius", "Eliossos", "Elantra", "Égiro", "Egito", "Esquadra", "Elvin", "Ethan", "Eugin", "Esperança", "Eomil", "Endorsal", "Esmeraldin",
"Evancar", "Fabri", "Fulham", "Fontes", "Fortes", "Feotonte", "Frugulhetti", "Frivolo", "Fan", "Florente", "Florín", "Fábula", "Flamel", "Findel", "Fuzambro", "Fadonte",
"Furiel", "Frambode", "Farias", "Fameton", "Fanto", "Firago", "Fiorella", "Fiametta", "Flamini", "Flint", "Gargaros", "Golanto", "Gamarra", "Garuma", "Graes", "Groe",
"Gâmbio", "Gurgel", "Garonte", "Golia", "Greviche", "Govan", "Gokan", "Garotto", "Gasol", "Giordanno", "Gertund", "Galfano", "Hora", "Helocobro", "Humanite", "Holdram",
"Idem", "Izam", "Island", "Igga", "Ieus", "Ilhéus", "Iuvina", "Ieronto", "Ildebert", "Ivanov", "Ishtar", "Jambo", "Jacar", "Jordemor", "Jarente", "Judice", "Japoné",
"Kremlin", "Kaiser", "Krioto", "Kabela", "Kamafeu", "Kastis", "Kubalua", "Kerrigan", "Kiav", "Keller", "Leonort", "Lamartine", "Lorias", "Lepostal", "Lavandri", "Lua",
"Laurent", "Lazarus", "Lépido", "Livina", "Loris", "Lótus", "Marduk", "Mauritânia", "Modré", "Maltan", "Mattar", "Mattos", "Madureira", "Marzupis", "Mordrigues", "Miatto",
"Melandrus", "Miralles", "Macnelly", "Mondego", "Marambaia", "Miro", "Milhorbom", "Maribe", "Malibu", "Nilz", "Nerruagem", "Neves", "Nevoeiro", "Naval", "Neotino", "Nardelli",
"Nugget", "Newson", "Nereido", "Númida", "Orange", "Opus", "Orthal", "Orfanus", "Ozeus"]
nomes[Prototipo::SERVANO][Nome::APELIDO_MASCULINO] = [
                                                { palavra: "Abutre",       genero: "m"},
                                                { palavra: "Albatroz",     genero: "m"},
                                                { palavra: "Fúria",        genero: "f"},
                                                { palavra: "Astrólogo",    genero: "m"},
                                                { palavra: "Colosso",      genero: "m"},
                                                { palavra: "Canibal",      genero: "m"},
                                                { palavra: "Papagaio",     genero: "m"},
                                                { palavra: "Cão",          genero: "m"},
                                                { palavra: "Cachorro",     genero: "m"},
                                                { palavra: "Gavião",       genero: "m"},
                                                { palavra: "Leão",         genero: "m"},
                                                { palavra: "Tubarão",      genero: "m"},
                                                { palavra: "Urso",         genero: "m"},
                                                { palavra: "Machado",      genero: "m"},
                                                { palavra: "Estátua",      genero: "f"},
                                                { palavra: "Vampiro",      genero: "m"},
                                                { palavra: "Gladiador",    genero: "m"},
                                                { palavra: "Lutador",      genero: "m"},
                                                { palavra: "Crocodilo",    genero: "m"},
                                                { palavra: "Rinoceronte",  genero: "m"},
                                                { palavra: "Corvo",        genero: "m"},
                                                { palavra: "General",      genero: "m"},
                                                { palavra: "Capitão",      genero: "m"},
                                                { palavra: "Sargento",     genero: "m"},
                                                { palavra: "Caveira",      genero: "f"},
                                                { palavra: "Punho",        genero: "m"},
                                                { palavra: "Mão",          genero: "f"},
                                                { palavra: "Animal",       genero: "m"},
                                                { palavra: "Rei",          genero: "m"},
                                                { palavra: "Conde",        genero: "m"},
                                                { palavra: "Barão",        genero: "m"},
                                                { palavra: "Visconde",     genero: "m"},
                                                { palavra: "Castor",       genero: "m"},
                                                { palavra: "Falcão",       genero: "m"},
                                                { palavra: "Marreta",      genero: "f"},
                                                { palavra: "Casca",        genero: "f"},
                                                { palavra: "Crosta",       genero: "f"},
                                                { palavra: "Macaco",       genero: "m"},
                                                { palavra: "Gorila",       genero: "m"},
                                                { palavra: "Grilo",        genero: "m"},
                                                { palavra: "Gigante",      genero: "m"},
                                                { palavra: "Soldado",      genero: "m"},
                                                { palavra: "Arconde",      genero: "m"},
                                                { palavra: "Morcego",      genero: "m"},
                                                { palavra: "Martelo",      genero: "m"},
                                                { palavra: "Arma",         genero: "f"},
                                                { palavra: "Cavalo",       genero: "m"},
                                                { palavra: "Jumento",      genero: "m"},
                                                { palavra: "Gnu",          genero: "m"},
                                                { palavra: "Elefante",     genero: "m"},
                                                { palavra: "Bebedor",      genero: "m"},
                                                { palavra: "Comedor",      genero: "m"},
                                                { palavra: "Cratera",      genero: "f"},
                                                { palavra: "Verme",        genero: "m"},
                                                { palavra: "Trovão",       genero: "m"},
                                                { palavra: "Verme",        genero: "m"},
                                                { palavra: "Força",        genero: "f"},
                                                { palavra: "Guerreiro",    genero: "m"},
                                                { palavra: "Grifo",        genero: "m"},
                                                { palavra: "Sátiro",       genero: "m"},
                                                { palavra: "Pavão",        genero: "m"},
                                                { palavra: "Agente",       genero: "m"},
                                                { palavra: "Primata",      genero: "m"},
                                                { palavra: "Torre",        genero: "f"},
                                                { palavra: "Cortador",     genero: "m"},
                                                { palavra: "Foice",        genero: "f"},
                                                { palavra: "Alicate",      genero: "m"},
                                                { palavra: "Peso",         genero: "m"},
                                                { palavra: "Destruidor",   genero: "m"},
                                                { palavra: "Invasor",      genero: "m"},
                                                { palavra: "Escorpião",    genero: "m"},
                                                { palavra: "Tufão",        genero: "m"},
                                                { palavra: "Campeão",      genero: "m"},
                                                { palavra: "Ladrão",       genero: "m"},
                                                { palavra: "Vigarista",    genero: "m"},
                                                { palavra: "Desafiador",   genero: "m"},
                                                { palavra: "Corredor",     genero: "m"},
                                                { palavra: "Perseguidor",  genero: "m"},
]
nomes[Prototipo::SERVANO][Nome::APELIDO_FEMININO] = [
                                                { palavra: "Gazela",     genero: "f"},
                                                { palavra: "Ameaça",     genero: "f"},
                                                { palavra: "Astúcia",    genero: "f"},
                                                { palavra: "Afronta",    genero: "f"},
                                                { palavra: "Amanhecer",  genero: "m"},
                                                { palavra: "Camaleoa",   genero: "f"},
                                                { palavra: "Coruja",     genero: "f"},
                                                { palavra: "Estrela",    genero: "f"},
                                                { palavra: "Fantasma",   genero: "m"},
                                                { palavra: "Lingua",     genero: "f"},
                                                { palavra: "Andorinha",  genero: "f"},
                                                { palavra: "Abelha",     genero: "f"}, 
                                                { palavra: "Cabrita",    genero: "f"},
                                                { palavra: "Mondragora", genero: "f"},
                                                { palavra: "Borboleta",  genero: "f"},
                                                { palavra: "Brisa",      genero: "f"},
                                                { palavra: "Fada",       genero: "f"},
                                                { palavra: "Serpente",   genero: "f"},
                                                { palavra: "Cobra",      genero: "f"},
                                                { palavra: "Leoa",       genero: "f"},
                                                { palavra: "Tigresa",    genero: "f"},
                                                { palavra: "Ave",        genero: "f"},
                                                { palavra: "Lua",        genero: "f"},
                                                { palavra: "Raio",       genero: "m"},
                                                { palavra: "Seta",       genero: "f"},
                                                { palavra: "Velocidade", genero: "f"},
                                                { palavra: "Aranha",     genero: "f"},
                                                { palavra: "Esquila",    genero: "f"},
                                                { palavra: "Mureta",     genero: "f"},
                                                { palavra: "Arara",      genero: "f"},
                                                { palavra: "Espinho",    genero: "m"},
                                                { palavra: "Pena",       genero: "f"},
                                                { palavra: "Árvore",     genero: "f"},
                                                { palavra: "Seda",       genero: "f"},
                                                { palavra: "Corte",      genero: "m"},
                                                { palavra: "Lâmina",     genero: "f"},
                                                { palavra: "Adaga",      genero: "f"},
                                                { palavra: "Esmeralda",  genero: "f"},
                                                { palavra: "Rubi",       genero: "m"},
                                                { palavra: "Lula",       genero: "f"},
                                                { palavra: "Diamante",   genero: "m"},
                                                { palavra: "Topázio",    genero: "m"},
                                                { palavra: "Ametísta",   genero: "f"}, 
                                                { palavra: "Safira",     genero: "f"},
                                                { palavra: "Golpe",      genero: "m"},
                                                { palavra: "Onda",       genero: "f"},
                                                { palavra: "Asa",        genero: "f"},
                                                { palavra: "Salto",      genero: "m"},
                                                { palavra: "Espada",     genero: "f"},
                                                { palavra: "Veneno",     genero: "m"},
                                                { palavra: "Planeta",    genero: "m"},
                                                { palavra: "Olho",       genero: "m"}, 
                                                { palavra: "Unha",       genero: "f"},
                                                { palavra: "Mergulho",   genero: "m"},
                                                { palavra: "Lagarta",    genero: "f"},
                                                { palavra: "Centopéia",  genero: "f"},
                                                { palavra: "Tartaruga",  genero: "f"},
                                                { palavra: "Zebra",      genero: "f"},
                                                { palavra: "Lança",      genero: "f"},
                                                { palavra: "Corredora",  genero: "f"},
                                                { palavra: "Esfínge",    genero: "f"},
                                                { palavra: "Avalanche",  genero: "f"},
                                                { palavra: "Capitã",     genero: "f"},
                                                { palavra: "Major",      genero: "f"},
                                                { palavra: "Comandante", genero: "f"},
                                                { palavra: "Palma",      genero: "f"},
                                                { palavra: "Testa",      genero: "f"},
                                                { palavra: "Gata",       genero: "f"},
                                                { palavra: "Mariposa",   genero: "f"},
                                                { palavra: "Raposa",     genero: "f"},
                                                { palavra: "Prisma",     genero: "m"},
                                                { palavra: "Cabana",     genero: "f"},
                                                { palavra: "Sereia",     genero: "f"},
                                                { palavra: "Mito",       genero: "m"},
                                                { palavra: "Medusa",     genero: "f"},
                                                { palavra: "Cometa",     genero: "m"},
                                                { palavra: "Asteróide",  genero: "m"},
                                                { palavra: "Girafa",     genero: "f"},
                                                { palavra: "Galáxia",    genero: "f"},
                                                { palavra: "Diva",       genero: "f"},
                                                { palavra: "Rainha",     genero: "f"},
                                                { palavra: "Baronesa",   genero: "f"},
                                                { palavra: "Condessa",   genero: "f"},
                                                { palavra: "Baleia",     genero: "f"},
                                                { palavra: "Suserana",   genero: "f"},
                                                { palavra: "Matriarca",  genero: "f"},
                                                { palavra: "Arte",       genero: "f"},
                                                { palavra: "Guerreira",  genero: "f"},
                                                { palavra: "Música",     genero: "f"},
                                                { palavra: "Sinfonia",   genero: "f"},
                                                { palavra: "Ursa",       genero: "f"},
                                                { palavra: "Onça",       genero: "f"},
                                                { palavra: "Loba",       genero: "f"},
                                                { palavra: "Canguro",    genero: "m"},
                                                { palavra: "Felina",     genero: "f"},
                                                { palavra: "Usurpadora", genero: "f"},
                                                
]
nomes[Prototipo::SERVANO][Nome::ADJETIVO] = [
                                                { palavra: "Azul",       genero: "u"},
                                                { palavra: "Verde",      genero: "u"},
                                                { palavra: "Cinza",      genero: "u"},
                                                { palavra: "Marrom",     genero: "u"},
                                                { palavra: "Vermelho",   genero: "m"},
                                                { palavra: "Vermelha",   genero: "f"},
                                                { palavra: "Amarelo",    genero: "m"},
                                                { palavra: "Amarela",    genero: "f"},
                                                { palavra: "Branco",     genero: "m"},
                                                { palavra: "Branca",     genero: "f"},
                                                { palavra: "Preto",      genero: "m"},
                                                { palavra: "Preta",      genero: "f"},
                                                { palavra: "Negro",      genero: "m"},
                                                { palavra: "Negra",      genero: "f"},
                                                { palavra: "Rubro",      genero: "m"},
                                                { palavra: "Rubra",      genero: "f"},
                                                { palavra: "Roxo",       genero: "m"},
                                                { palavra: "Roxa",       genero: "f"},
                                                { palavra: "de Pedra",   genero: "u"},
                                                { palavra: "de Sangue",  genero: "u"},
                                                { palavra: "de Barro",   genero: "u"},
                                                { palavra: "de Madeira", genero: "u"},
                                                { palavra: "de Mármore", genero: "u"},
                                                { palavra: "de Marfim",  genero: "u"},
                                                { palavra: "de Ferro",   genero: "u"},
                                                { palavra: "de Cobre",   genero: "u"},
                                                { palavra: "de Prata",   genero: "u"},
                                                { palavra: "de Ouro",    genero: "u"},
                                                { palavra: "da Terra",   genero: "u"},
                                                { palavra: "do Ar",      genero: "u"},
                                                { palavra: "do Fogo",    genero: "u"},
                                                { palavra: "da Água",    genero: "u"},
                                                { palavra: "Raivoso",    genero: "m"},
                                                { palavra: "Raivosa",    genero: "f"},
                                                { palavra: "Rancoroso",  genero: "m"},
                                                { palavra: "Rancorosa",  genero: "f"},
                                                { palavra: "Rápido",     genero: "m"},
                                                { palavra: "Rápida",     genero: "f"},
                                                { palavra: "Ligeiro",    genero: "m"},
                                                { palavra: "Ligeira",    genero: "f"},
                                                { palavra: "Selvagem",   genero: "u"},
                                                { palavra: "Sagaz",      genero: "u"},
                                                { palavra: "Dominador",  genero: "m"},
                                                { palavra: "Dominadora", genero: "f"},
                                                { palavra: "Controladora", genero: "f"},
                                                { palavra: "Controlador", genero: "m"},
                                                { palavra: "Pesado",     genero: "m"},
                                                { palavra: "Destruidora",  genero: "f"},
                                                { palavra: "Destruidor", genero: "m"},
                                                { palavra: "Jovem",      genero: "u"},
                                                { palavra: "Forte",      genero: "u"},
                                                { palavra: "Gritante",   genero: "u"},
                                                { palavra: "Mítico",     genero: "m"},
                                                { palavra: "Lendária",   genero: "f"},
                                                { palavra: "da Guerra",  genero: "u"},
                                                { palavra: "da Noite",   genero: "u"},
                                                { palavra: "da Luz",     genero: "u"},
                                                { palavra: "Eterno",     genero: "m"},
                                                { palavra: "Eterna",     genero: "f"},
                                                { palavra: "Dourada",    genero: "f"},
                                                { palavra: "Dourado",    genero: "m"},
                                                { palavra: "Prateada",   genero: "f"},
                                                { palavra: "Prateado",   genero: "m"},
                                                { palavra: "Demente",    genero: "u"},
                                                { palavra: "Louca",      genero: "f"},
                                                { palavra: "Louco",      genero: "m"},
                                                { palavra: "Perturbado", genero: "m"},
                                                { palavra: "Perturbada", genero: "f"},
                                                { palavra: "Cósmica",    genero: "f"},
                                                { palavra: "Zodíaco",    genero: "m"},
                                                { palavra: "Zodíaca",    genero: "f"},
                                                { palavra: "Astral",     genero: "u"},
                                                { palavra: "Amaldiçoado", genero: "m"},
                                                { palavra: "Amaldiçoada", genero: "f"},
                                                { palavra: "Desgraçada", genero: "f"},
                                                { palavra: "Desgraçado", genero: "m"},
                                                { palavra: "Violento",   genero: "m"},
                                                { palavra: "Violenta",   genero: "f"},
                                                { palavra: "Arrebatador", genero: "m"},
                                                { palavra: "Arrebatadora", genero: "f"},
                                                { palavra: "Viril",      genero: "m"},
                                                { palavra: "Esmagador",  genero: "m"},
                                                { palavra: "Esmagadora", genero: "f"},
                                                { palavra: "Silvestre",  genero: "u"},
                                                { palavra: "Florestal",  genero: "u"},
                                                { palavra: "Cavernoso",  genero: "m"},
                                                { palavra: "Cavernosa",  genero: "f"},
                                                { palavra: "Pantaneiro", genero: "m"},
                                                { palavra: "Pantaneira", genero: "f"},
                                                { palavra: "Vingador",   genero: "m"},
                                                { palavra: "Vingadora",  genero: "f"},
                                                { palavra: "Vingativo",  genero: "m"},
                                                { palavra: "Vingativa",  genero: "f"},
                                                { palavra: "Traiçoeiro", genero: "m"},
                                                { palavra: "Traiçoeira", genero: "f"},
                                                { palavra: "Gringo",     genero: "m"},
                                                { palavra: "Gringa",     genero: "f"},
                                                { palavra: "Sóbrio",     genero: "m"},
                                                { palavra: "Sóbria",     genero: "f"},
                                                { palavra: "Insinuante", genero: "u"},
                                                { palavra: "Sinistro",   genero: "m"},
                                                { palavra: "Sinistra",   genero: "f"},
                                                { palavra: "Maldoso",    genero: "m"},
                                                { palavra: "Maldosa",    genero: "f"},
                                                { palavra: "Espectral",  genero: "u"},
                                                { palavra: "Radiante",   genero: "u"},
                                                { palavra: "Neurótico",  genero: "m"},
                                                { palavra: "Neurótica",  genero: "f"},
                                                { palavra: "Perverso",   genero: "m"},
                                                { palavra: "Perversa",   genero: "f"},
                                                { palavra: "Firme",      genero: "u"},
                                                { palavra: "Errante",    genero: "u"},
                                                { palavra: "Tortuoso",   genero: "m"},
                                                { palavra: "Tortuosa",   genero: "f"},
                                                { palavra: "Agressivo",  genero: "m"},
                                                { palavra: "Agressiva",  genero: "f"},
                                                { palavra: "da Montanha", genero: "u"},
                                                { palavra: "do Vale",    genero: "u"},
                                                { palavra: "da Colina",  genero: "u"},
                                                { palavra: "do Deserto", genero: "u"},
                                                
                                                
                                                
]

=begin
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Galindo"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bastião"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Âmber"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Casigoto"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Humbra"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Murento"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Malicioso"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Vermelho"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Leitor de Mentes"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Cador"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bugar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Juju"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Zorrá"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Ayê"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Kabaral"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Sacador de Costela"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Bruto"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Mastigador de Ossos"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bubagol"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Ygar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bugalu"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Gorrash"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Ayê"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Sossak"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Fuçador de Fígado"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Mordedor"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Dentão"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bubador"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Yvar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Gugaj"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Suduh"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Tabadum"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Sossak"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Sagaz"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Fatiador de Pescoços"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Problema"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Gudru"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bodram"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Vorrak"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Bakar"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Kashkar"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Sossak"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Garganta"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Tora"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Guerreiro Verde"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Alavim"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Furiel"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Skazi"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Galaas"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Shanvi"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Pavam"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Flash"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cabelo de Prata"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Jar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Ur"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Jamal"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Garnesh"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Nuudal"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Mavr"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Jamanta"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Eviscerador"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Gigante Cinza"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Mephisto"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Balthazar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Balor"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Infernus"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Diabolo"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Andremmelech"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Barata Cascuda"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Capiroto"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Chifrudo"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Alan"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Cassius"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Talandre"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Vendrano"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Martel"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Olic"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Mão-de-Marte"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Vingador"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Rebelde"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Don"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Ralf"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Diogo"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Raska"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Wasp"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Krol"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Martelo da Morte"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cabeça de Ferro"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Gladiador de Mármore"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Ivan"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Andrei"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Klaus"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Karkiv"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Mondragon"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Kiev"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Homem de Ferro"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Tanque"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Tubarão"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Dario"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Carlos"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Raul"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Castelo"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Durio"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Cavani"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cavalo Espanhol"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cor-de-Sangue"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Caçador"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Gorrash"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Kar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Morrash"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Tyiê"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Dabur"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Shudu"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Urro Berro"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Fúria Viva"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Crocodilo"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Yoshi"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Hidemasa"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Kaneto"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Sakuraba"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Satoru"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Yamaha"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Vento Leve"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Tsunami"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Samurai"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Nohaj"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Azmodan"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Caim"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Necronomicam"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Sangr"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Onulukam"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Senhor da Perdição"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Boizebú"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cão"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Alex"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Rodrik"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Tales"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Vanguart"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Vermillion"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Avalon"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Escudo do Norte"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Espada Cromada"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Lobo"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Yuri"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Leo"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Axel"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Wei"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Wan"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Cho"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "General de Guerra"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Raio"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Tigre"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Davon"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Loxis"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Zul"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Durahan"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Tarquin"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Solaris"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Guardião"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cratera"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Rinoceronte"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Rasho"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Lozzis"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Ragor"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Dabarum"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Rossack"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Shudu"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Destruidor"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Golpe Baixo"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Rasga-Torso"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Alfredo"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Jean"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Charles"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Neuville"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Frontignan"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Saint-Gilles"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Francês"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Visionário"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Instável"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Gorupta"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Mukagohopta"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Morrupa"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Barkana"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Shamel"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Gahana"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Búfalo"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Búfalo Bill"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Shamã"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bogran"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Groc"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Korrash"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Mogrel"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Gotar"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Kawar"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Meio-Orc"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Barão"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Marreta"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Eric"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Arvin"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Jun"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Grabber"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Lumen"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Agatar"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Especialista"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Droid"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Flecha"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Marco"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bojan"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Corraj"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Tushu"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Dabur"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Barkash"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Lutador"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Radar"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Porradeiro"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Ariel"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Muriel"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Zola"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Ganesh"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Lutrand"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Zirkam"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Homem de Gelo"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Vento Ártico"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Congelador"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Dongo"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Jumar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Botram"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Sossak"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Kaleg"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Xokram"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Defensor"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cabeça Dinossauro"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Porco Verde"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Marcel"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Frederico"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Zorei"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Porto"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Woodgate"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Dravinsk"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Espada Branca"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Caju"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Veloz"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Osmar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Ari"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Durval"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Roman"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Umbral"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Gaskin"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Honrado"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Montanha"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "General de Ferro"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Dominick"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Dohamir"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Balhara"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Solax"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Zeel"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Neptano"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Arcano"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Hocus Pocus"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Caçador de Cometas"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Atlas"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Kerman"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Fernando"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Karmic"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Duranol"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Durio"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Lobo de Ferro"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Titânio"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Soldado de Carga"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Keldon"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Lima"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Vinny"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Keane"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Meliandre"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Jimbo"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Garoto"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Névoa"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Raio da Lua"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Batrok"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Androkkar"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Goliah"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Murragorash"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Borgan"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Drakkar"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Capitão da Armada"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Machado Violento"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Implacável"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Grom"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Kim"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Takrash"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Krakav"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Omungadolo"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Brogo"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Fanático"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Sangue no Zói"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Besta"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Elder"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Bóris"
nomes[Prototipo::NEFANO][Nome::MASCULINO] << "Nicolau"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Mandrake"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Garlic"
nomes[Prototipo::NEFANO][Nome::SOBRENOME] <<  "Vendeta"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Cruel"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Barba Ruiva"
nomes[Prototipo::NEFANO][Nome::APELIDO_MASCULINO] <<  "Maldoso"

=end
carregar_retratos("/app/assets/images")
salvar_nomes(nomes)
